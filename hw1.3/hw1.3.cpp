﻿#include <iostream>
#include <cmath>

class Vector {
public:
    Vector() {
        x = 0; y = 0; z = 0;
    }

    Vector(const float x, const float y, const float z) {
        this->x = x; this->y = y; this->z = z;
    }

    friend std::ostream& operator << (std::ostream& out, const Vector& a);
    friend std::istream& operator >> (std::istream& input, Vector& a);

    bool friend operator > (const Vector& a, const Vector& b);
    bool friend operator < (const Vector& a, const Vector& b);

    Vector friend operator + (const Vector& a, const Vector& b);
    Vector friend operator - (const Vector& a, const Vector& b);
    Vector friend operator * (const Vector& a, float b);
    float friend operator * (const Vector& a, const Vector& b);

    float operator [] (int index);

private:
    float x;
    float y;
    float z;
};

float Vector::operator [] (int index)
{
    switch (index)
    {
    case 0:
        return x;
    case 1:
        return y;
    case 2:
        return z;
    default:
        return 0;
    }
}

std::ostream& operator << (std::ostream& out, const Vector& a)
{
    out << '[' << a.x << ", " << a.y << ", " << a.z << ']';
    return out;
}

std::istream& operator >> (std::istream& input, Vector& a)
{
    input >> a.x >> a.y >> a.z;
    return input;
}

bool operator > (const Vector& a, const Vector& b)
{
    return fabs(a.x) + fabs(a.y) + fabs(a.z) > fabs(b.x) + fabs(b.y) + fabs(b.z);
}

bool operator < (const Vector& a, const Vector& b)
{
    return fabs(a.x) + fabs(a.y) + fabs(a.z) < fabs(b.x) + fabs(b.y) + fabs(b.z);
}

Vector operator + (const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator-(const Vector& a, const Vector& b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

float operator * (const Vector& a, const Vector& b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector operator * (const Vector& a, const float b)
{
    return Vector(a.x * b, a.y * b, a.z * b);
}

int main()
{
    Vector a;
    Vector b;

    std::cout << "init a:" << std::endl;
    std::cin >> a;
    std::cout << "init b:" << std::endl;
    std::cin >> b;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "a * b = " << a * b << std::endl;
    std::cout << "a * 2 = " << a * 2 << std::endl;
    std::cout << "a + b = " << a + b << std::endl;
    std::cout << "a - b = " << a - b << std::endl;
    std::cout << "a > b = " << (a > b) << std::endl;
    std::cout << "a < b = " << (a < b) << std::endl;
}
