#pragma once
#include "UnitType.h"
#include "Weapon.h"

class Unit
{
    Weapon* weapon = nullptr;

    // for flyweight
    UnitType* unit_type = nullptr;

public:
    Unit() = default;
    // for flyweight
    Unit(EUnitType type, Model* model, Texture* texture);

    ~Unit();

    void TakeWeapon(Weapon* new_weapon);
    void Fire();
    void Reload();

};
