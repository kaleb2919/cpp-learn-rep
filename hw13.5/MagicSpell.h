#pragma once

class MagicSpell
{
public:
    virtual ~MagicSpell() = default;
    virtual void Execute();
};
