#pragma once

class UIElement
{
public:
    virtual void Draw() = 0;
};
