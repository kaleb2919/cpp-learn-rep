#include "DataBase.h"

#include <iostream>

Data* DataBase::RequestData(std::string &type)
{
    return new Data(type);
}

Data* DataBase::LazyLoadData(std::string &type)
{
    for (auto data : loaded_data)
    {
        if (data->isEqual(type))
        {
            std::cout << "Return cached data" << std::endl;
            return data;
        }
    }

    std::cout << "Return new data" << std::endl;
    auto data = RequestData(type);
    loaded_data.push_back(data);
    return data;
}
