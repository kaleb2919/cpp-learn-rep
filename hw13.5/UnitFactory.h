#pragma once

#include <vector>

#include "UnitType.h"


class UnitFactory
{
    static std::vector<UnitType*> unit_types;

public:
    static UnitType* getUnitType(EUnitType unit_type, Model* model, Texture* texture);

};
