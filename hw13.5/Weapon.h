#pragma once

class Weapon
{
public:
    virtual ~Weapon() = default;
    virtual void Fire() = 0;
    virtual void Reload() = 0;
};
