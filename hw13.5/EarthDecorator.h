#pragma once
#include "BaseMagicDecorator.h"

class EarthDecorator : public BaseMagicDecorator
{
public:
    explicit EarthDecorator(MagicSpell* magic_spell)
        : BaseMagicDecorator(magic_spell)
    {
    }

    void Execute() override;
};
