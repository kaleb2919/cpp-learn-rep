#include "UnitFactory.h"

#include <iostream>
#include <vector>

std::vector<UnitType*> UnitFactory::unit_types;

UnitType* UnitFactory::getUnitType(EUnitType type, Model* model, Texture* texture)
{
    for (auto unit_type : unit_types)
    {
        if (unit_type->isEqual(type, model, texture))
        {
            std::cout << "Reused type" << std::endl;
            return unit_type;
        }
    }

    std::cout << "New type" << std::endl;
    auto unit_type = new UnitType(type, model, texture);
    unit_types.push_back(unit_type);
    return unit_type;
}
