#pragma once

enum EUnitType
{
    Warrior,
    Mage,
    Rogue
};

class Texture { };
class Model { };

class UnitType
{
    EUnitType type;
    Model* model;
    Texture* texture;

public:
    UnitType(EUnitType type, Model* model, Texture* texture);
    bool isEqual(EUnitType other_type, const Model* other_model, const Texture* other_texture) const;
};
