#pragma once
#include <vector>
#include <string>

#include "Data.h"

class DataBase
{
    std::vector<Data*> loaded_data;

public:
    Data* RequestData(std::string &type);
    Data* LazyLoadData(std::string &type);
};
