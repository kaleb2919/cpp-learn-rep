#include "Unit.h"

#include "UnitFactory.h"

Unit::Unit(EUnitType type, Model* model, Texture* texture)
{
    unit_type = UnitFactory::getUnitType(type, model, texture);
}

Unit::~Unit()
{
    delete weapon;
}

void Unit::TakeWeapon(Weapon* new_weapon)
{
    weapon = new_weapon;
}

void Unit::Fire()
{
    if (weapon)
    {
        weapon->Fire();
    }
}

void Unit::Reload()
{
    if (weapon)
    {
        weapon->Reload();
    }
}
