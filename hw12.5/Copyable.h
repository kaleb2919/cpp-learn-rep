#pragma once

class Copyable
{
public:
    virtual ~Copyable() = default;
    virtual Copyable* copy() = 0;
};
