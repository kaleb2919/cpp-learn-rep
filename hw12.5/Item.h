#pragma once
#include <string>

class Item
{
    std::string name;
public:
    Item(std::string name);
    std::string getName();
};
