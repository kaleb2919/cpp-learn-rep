#pragma once
#include <unordered_map>

// Условный класс подключения к базе данных
class DataBaseConnection
{
    inline static DataBaseConnection* storehouse;

    DataBaseConnection() {};

public:

    DataBaseConnection(DataBaseConnection& other) = delete;
    void operator=(const DataBaseConnection&) = delete;

    static DataBaseConnection* getInstance()
    {
        if (!storehouse)
        {
            storehouse = new DataBaseConnection();
        }

        return storehouse;
    }
};
