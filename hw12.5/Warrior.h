#pragma once
#include "Unit.h"

class Warrior : public Unit
{
public:
    Warrior(Outfit* outfit, Weapon* weapon);
};
