#pragma once
#include "Unit.h"
#include "Warrior.h"

class Mage: public Unit
{
public:
    Mage(Outfit* outfit, Weapon* weapon);
    Unit* createClone(Unit* unit);
    void heal(Unit* unit, unsigned short value);
};
