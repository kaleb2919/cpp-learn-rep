#include "Forge.h"

Outfit* Forge::createArmor()
{
    return new Outfit("Armor", 200);
}

Weapon* Forge::createStaff()
{
    return new Weapon("Staff", 120);
}

Outfit* Forge::createRobe()
{
    return new Outfit("Robe", 50);
}

Weapon* Forge::createSword()
{
    return new Weapon("Sword", 140);
}

Weapon* Forge::createMace()
{
    return new Weapon("Mace", 180);
}
