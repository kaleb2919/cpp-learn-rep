#include "LazyLoadStrategy.h"

#include <iostream>

Data* LazyLoadStrategy::execute(std::string& type)
{
    for (auto data : loaded_data)
    {
        if (data->isEqual(type))
        {
            std::cout << "Return cached data" << std::endl;
            return data;
        }
    }

    std::cout << "Return new data" << std::endl;
    auto data = new Data(type);
    loaded_data.push_back(data);
    return data;
}
