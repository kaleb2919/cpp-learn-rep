#pragma once
#include <vector>

#include "Subscriber.h"

class EventManager
{
    std::vector<Subscriber*> subscribers;
public:
    void subscribe(Subscriber* client);
    void fireEvent();
};
