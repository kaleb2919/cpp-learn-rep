#pragma once
#include <string>

class State;
class Visitor;

class Unit
{
    State* grade = nullptr;
    std::string name;
public:
    Unit(std::string name);

    std::string getName();
    void upgrade();
    void attack();
    virtual void accept(Visitor* visitor) = 0;
};
