#pragma once
#include "State.h"

class NoviceState : public State
{
public:
    NoviceState(Unit* unit) : State(unit) { }

    void attack() override;
};
