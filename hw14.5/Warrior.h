#pragma once

#include "Unit.h"

class Warrior : public Unit
{
public:
    Warrior(std::string name) : Unit(name) { }

    void accept(Visitor* visitor) override;
};
