#include "Warrior.h"
#include "Visitor.h"

void Warrior::accept(Visitor* visitor)
{
    visitor->visit(this);
}
