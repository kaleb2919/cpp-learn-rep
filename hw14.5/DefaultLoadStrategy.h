#pragma once
#include "Strategy.h"

class DefaultLoadStrategy : public Strategy
{
public:
    Data* execute(std::string& type) override;
};
