#include "Unit.h"
#include <iostream>

#include "NoviceState.h"
#include "VeteranState.h"

Unit::Unit(std::string name) : name(name)
{
    std::cout << "Unit::created name = " << name << std::endl;
    grade = new NoviceState(this);
}

std::string Unit::getName()
{
    return name;
}

void Unit::upgrade()
{
    std::cout << "Unit::upgrade from NoviceState to VeteranState" << std::endl;
    grade = new VeteranState(this);
}

void Unit::attack()
{
    grade->attack();
}
