#include "DefaultLoadStrategy.h"

#include <iostream>

Data* DefaultLoadStrategy::execute(std::string& type)
{
    std::cout << "Return new data" << std::endl;
    return new Data(type);
}
