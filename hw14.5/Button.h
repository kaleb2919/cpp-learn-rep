#pragma once
#include "Command.h"

class Button
{
    Command* command = nullptr;
public:
    Button(Command* command);
    Command* getCommand();
};
